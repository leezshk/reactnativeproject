import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Modal, Image, ScrollView} from 'react-native';
import { Header, Tabs, Tab} from 'react-native-elements';

import downloadIcon from './assets/download.png';
import closeBtn from './assets/close.png';
import Brochures from './Components/Brochures';
import DataBooks from './Components/DataBooks';
import Webview from './Components/Webview';

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            selectedTab: 'Brochures',
            modalVisible: false
        }
    }

    changeTab(selectedTab) {
        this.setState({selectedTab});
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        const { selectedTab } = this.state;

        this.state = {
            downloadIcon: downloadIcon,
            closeBtn: closeBtn,
            selectedTab: this.state.selectedTab,
            modalVisible: this.state.modalVisible
        };

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Header
                        centerComponent={{ text: 'Tyres', style: { color: '#fff', fontWeight: 'bold', fontSize: 14} }}
                        rightComponent={{icon: 'info-outline', color: '#fff', marginBottom: -5, onPress: () => this.setModalVisible(true)}}
                    />
                </View>
                <View>
                    <Tabs tabBarStyle={styles.bgColorOrange}>
                        <Tab
                             titleStyle={styles.tabTitle}
                             selectedTitleStyle={styles.selectedTab}
                             tabStyle={selectedTab === 'Brochures' ?  styles.selectedBorderTab: styles.borderTab}
                              selected={selectedTab === 'Brochures'}
                             title="Brochures"
                             onPress={() => this.changeTab('Brochures')}>
                            <Webview/>
                        </Tab>
                        <Tab
                            titleStyle={styles.tabTitle}
                            selectedTitleStyle={styles.selectedTab}
                            selected={selectedTab === 'Databooks'}
                            tabStyle={selectedTab === 'Databooks' ?  styles.selectedBorderTab: styles.borderTab}
                            title="Databooks"
                            onPress={() => this.changeTab('Databooks')}>
                            <DataBooks/>
                        </Tab>
                    </Tabs>
                </View>
                {this.state.selectedTab === 'Brochures' ? <Webview/> : <DataBooks/>}


                <Modal animationType = {"slide"} transparent = {true}
                       visible = {this.state.modalVisible}
                    // visible = {false}
                       onRequestClose = {() => { console.log("Modal has been closed.") } }>


                    <View style = {styles.modalContainer}>
                        <View  style={styles.modalImage}>
                            <Image
                                style={styles.imageContent}
                                source={this.state.downloadIcon}
                            />
                        </View>

                        <ScrollView showsVerticalScrollIndicator={false}>
                            <Text style={styles.modalText}>
                                White Gold began gaining popularity in the early 1900's as an alternative to platinum.
                                Platinum was steadily becoming more fashionable, but because of.
                            </Text>
                            <Text style={styles.modalText}>
                                For more than 10 years, Both McFadden, a 44- year old mother of three, lived with strange
                                leg sensations that were not only difficult to describe , but were also uncomfortable and disruptive.
                                "At night, I would be lying in bed, just on the verge of going to sleep,
                                and I would get these feelings in my."

                            </Text>
                        </ScrollView>

                        <TouchableOpacity onPress = {() => {this.setModalVisible(false)}}>
                            <View  style={styles.closeBtnContainer}>
                                <Image style={styles.closeBtn}
                                       source={this.state.closeBtn}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#f2902a',
        flex: 1
    },
    header: {
        paddingTop: 20,
        height: 120,
        backgroundColor:'#f2902a',
        borderBottomWidth: 1,
        borderBottomColor: '#f2902a',
        justifyContent: 'center',
        alignItems: 'center'
    },
    bgColorOrange:{
        backgroundColor:'#f2902a'
    },
    tabTitle:{
        fontWeight: 'bold',
        fontSize: 14,
        paddingBottom: 15,
        paddingTop: 5,
        color: '#ffffff'
    },
    selectedTab:{
        color: '#ffffff'
    },
    selectedBorderTab:{
        borderBottomWidth:2,
        borderBottomColor:'#ffffff'
    },
    borderTab:{
        borderBottomWidth:0.5,
        borderBottomColor:'#f2f2f2'
    },
    imageContent:{
        width: 50,
        height: 50
    },
    modalContainer:{
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        padding: 26,
        margin:20,
        borderWidth: 1,
        borderColor: '#f2f2f2',
        borderRadius: 15
    },
    modalImage:{
        padding: 30
    },
    modalText:{
        fontSize: 20,
        color: '#2C2C2C',
        lineHeight: 35
    },
    closeBtnContainer:{
        padding: 20
    },
    closeBtn:{
        width: 40,
        height: 40
    }
});
