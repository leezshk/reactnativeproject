import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image, ScrollView, ListView} from 'react-native';
import { AppRegistry } from 'react-native';
import GridView from 'react-native-easy-gridview';
import downloadIcon from '../assets/download.png'

export default class Brochures extends Component{
    state = {
        dataSource: undefined
    };

    renderRow(rowData){
        return (
            <View style={styles.item}>
                <View style={styles.imageContainerStyle}>
                    <Image source={{uri: rowData.image_url}}  style={styles.imageStyle}/>
                </View>
                <View  style={styles.textContainerStyle}>
                    <Image source={downloadIcon}  style={styles.downloadIcon}/>
                    <Text style={styles.itemText}>{rowData.title}</Text>
                </View>
            </View>
        );
    }

    render(){
        const list = [
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/852c4c070a.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/broschure\/1_2017\/TIANLI_Agriculture_1_2017_EN.pdf",
                "title": "Tyres for agriculture"
            },
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/cacde23fc6.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/broschure\/1_2017\/TIANLI_Forestry_1_2017_EN.pdf",
                "title": "Tyres for forestry"
            },
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/469c9b34a5.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/broschure\/1_2017\/TIANLI_Baumaschinen_1_2017_EN.pdf",
                "title": "Tyres for construction machinery"
            }
        ];

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        var data = list;

        this.state = {
            dataSource: ds.cloneWithRows(data),
            downloadIcon: downloadIcon,
        };
        return(
            <ScrollView  showsVerticalScrollIndicator={false}>
                <View style={styles.brochuresContainer}>
                    <View  style={styles.imageHolder}>
                        <Image
                            style={styles.imageContent}
                            source={this.state.downloadIcon}
                        />
                        <Text style={styles.textContent}>Brochures short description
                            Lorem ipsum dolor sit amet, veri prodesset comprehensam ut duo.
                            Has ne aperiam corpora repudiare, primis tractatos argumentum per id.
                            Magna causae vix at. Clita denique imperdiet ad mea, meis viris noster cu mei.
                        </Text>
                    </View>

                    <View style={styles.listContainer}>
                            <GridView
                                dataSource={this.state.dataSource}
                                renderRow={this.renderRow}
                                numberOfItemsPerRow={2}
                                removeClippedSubviews={false}
                                initialListSize={1}
                                pageSize={2}/>
                    </View>

                </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        backgroundColor: '#f2902a',
        padding: 10
    },
    item: {
        backgroundColor: '#ffffff',
        height: 155,
        borderRadius: 20,
        margin: 15,
        paddingVertical: 7,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageContainerStyle: {
        height: 97
    },
    imageStyle: {
        width: 100,
        height: '95%',
        margin: 5
    },
    textContainerStyle:{
        flexDirection: 'row',
        backgroundColor: '#fff',
        width: '100%',
        height: 58,
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius:20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    downloadIcon:{
        width: 15,
        height: 15,
        marginRight: 7,
        marginTop: 5
    },
    itemText:{
        fontSize: 18,
        color: '#000',
        flexDirection: 'column'
    },
    brochuresContainer:{
        padding: 20
    },
    imageHolder:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageContent:{
        width: 50,
        height: 50
    },
    textContent:{
        fontSize: 20,
        padding: 15,
        color: '#ffffff'
    },
    cardContainer:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        flexWrap: 'wrap'
    }
});

AppRegistry.registerComponent('project', () => Brochures);