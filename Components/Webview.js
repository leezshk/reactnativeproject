import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image, ScrollView, Modal, ListView, WebView } from 'react-native';
import { AppRegistry } from 'react-native';


export default class Webview extends Component{
    render(){
        return(
            <View style={{flex: 1}}>
                <WebView source={{uri: 'http://www.tianli-tyre.com/typo3temp/pics/2515dcb0d7.jpg'}}
                         style={{flex: 1}} // OR style={{height: 100, width: 100}}
                />
            </View>
        );
    }
}

AppRegistry.registerComponent('project', () => Webview);