import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image, ScrollView, Modal, ListView, WebView } from 'react-native';
import { AppRegistry } from 'react-native';
import GridView from 'react-native-easy-gridview';

import downloadIcon from '../assets/download.png'
import closeBtn from '../assets/close.png';
import downloadWhite from '../assets/download-white.png';
import Webview from './Webview';

export default class DataBooks extends Component{

    constructor(props) {
        super(props);
    }

    state = {
        modalVisible: false,
        dataSource: undefined,
        openWeb: false
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    openBrowser(url){
        console.log("open this",url);
        this.setState({openWeb: true});
    }

    renderRow(rowData){
        return (
            <View style={styles.item}>
                <TouchableOpacity onPress={() => this.openBrowser(rowData.image_url)}>
                    <View style={styles.imageContainerStyle}>
                        <Image source={{uri: rowData.image_url}}  style={styles.imageStyle}/>
                    </View>
                </TouchableOpacity>
                <View  style={styles.textContainerStyle}>
                    <Image source={downloadWhite}  style={styles.downloadIcon}/>
                    <Text style={styles.itemText}>{rowData.title}</Text>
                </View>
            </View>
        );
    }

    render(){
        const list = [
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/2515dcb0d7.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/databooks\/TIANLI_DATABOOK_Agriculture_AgroGrip_1_2017_EN_Webversion_v2.0.pdf",
                "title": "Agro Grip"
            },
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/92fead4d08.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/databooks\/TIANLI_DATABOOK_Agriculture_F1Traction_1_2017_EN_Webversion_v2.0.pdf",
                "title": "F1 Traction"
            },
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/09ff544408.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/databooks\/TIANLI_DATABOOK_Agriculture_GrassKing_1_2017_EN_Webversion_v2.0.pdf",
                "title": "Grass King"
            },
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/c490af6435.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/databooks\/TIANLI_DATABOOK_Agriculture_Harvester_1_2017_EN_Webversion_v2.0.pdf",
                "title": "Harvester"
            },
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/8019caf894.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/databooks\/TIANLI_DATABOOK_Agriculture_R1Defender_1_2017_EN_Webversion_v2.0.pdf",
                "title": "R1 Defender"
            },
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/202a6060f0.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/databooks\/TIANLI_DATABOOK_Agriculture_R1W65_1_2017_EN_Webversion_v2.0.pdf",
                "title": "R1W 65"
            },
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/3685bc7b56.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/databooks\/TIANLI_DATABOOK_Agriculture_R1W80_85_90_Genesis85_95_1_2017_EN_Webversion_v2.0.pdf",
                "title": "R1W 80\/85\/90"
            },
            {
                "image_url": "http:\/\/www.tianli-tyre.com\/typo3temp\/pics\/36da1760ee.jpg",
                "pdf_url": "http:\/\/www.tianli-tyre.com\/fileadmin\/content\/en\/databooks\/TIANLI_DATABOOK_Agriculture_R4_1_2017_EN_Webversion_v2.0.pdf",
                "title": "R1 Genesis 85\/95"
            }
        ];

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        var data = list;

        this.state = {
            dataSource: ds.cloneWithRows(data),
            downloadIcon: downloadIcon,
            closeBtn: closeBtn,
            modalVisible: this.state.modalVisible,
            openWeb: this.state.openWeb
        };

        return (
            this.state.openWeb ? <Webview /> : // pass url as props here in Webview
            <ScrollView  showsVerticalScrollIndicator={false}>
                <View style={styles.databooksContainer}>
                    <View  style={styles.imageHolder}>
                        <Image
                            style={styles.imageContent}
                            source={this.state.downloadIcon}
                        />
                        <Text style={styles.textContent}>
                            Databooks short description.
                            Lorem ipsum dolor sit amet, veri prodesset comprehensam ut duo.
                            Has ne aperiam corpora repudiare, primis tractatos argumentum per id.
                        </Text>
                    </View>
                </View>

                <View style={styles.listContainer}>
                    {/*<TouchableOpacity onPress={() => this.openBrowser('http://www.tianli-tyre.com/typo3temp/pics/2515dcb0d7.jpg')}>*/}
                        <GridView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow}
                            numberOfItemsPerRow={2}
                            removeClippedSubviews={false}
                            initialListSize={1}
                            pageSize={2}
                        />
                    {/*</TouchableOpacity>*/}

                </View>

                <Modal animationType = {"slide"} transparent = {true}
                       visible = {this.state.modalVisible}
                    // visible = {false}
                       onRequestClose = {() => { console.log("Modal has been closed.") } }>


                    <View style = {styles.modalContainer}>
                        <View  style={styles.modalImage}>
                            <Image
                                style={styles.imageContent}
                                source={this.state.downloadIcon}
                            />
                        </View>

                        <ScrollView showsVerticalScrollIndicator={false}>
                            <Text style={styles.modalText}>
                                White Gold began gaining popularity in the early 1900's as an alternative to platinum.
                                Platinum was steadily becoming more fashionable, but because of.
                                For more than 10 years, Both McFadden, a 44- year old mother of three, lived with strange
                                leg sensations that were not only difficult to describe , but were also uncomfortable and disruptive.
                                "At night, I would be lying in bed, just on the verge of going to sleep,
                                and I would get these feelings in my."

                            </Text>
                        </ScrollView>

                        <TouchableOpacity onPress = {() => {this.setModalVisible(false)}}>
                            <View  style={styles.closeBtnContainer}>
                                <Image style={styles.closeBtn}
                                       source={this.state.closeBtn}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        backgroundColor: '#f2902a',
        padding: 10
    },
    item: {
        backgroundColor: '#ffffff',
        height: 215,
        borderRadius: 20,
        margin: 15,
        paddingVertical: 7,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageContainerStyle: {
        height: 155
    },
    imageStyle: {
        width: 100,
        height: '95%',
        margin: 5
    },
    textContainerStyle:{
        flexDirection: 'row',
        backgroundColor: '#000',
        width: '100%',
        height: 60,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius:20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    downloadIcon:{
        width: 15,
        height: 15,
        marginRight: 5,
        marginTop: 5
    },
    itemText:{
        fontSize: 18,
        color: '#fff',
        flexDirection: 'column'
    },
    databooksContainer:{
        paddingTop: 20
    },
    imageHolder:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageContent:{
        width: 50,
        height: 50
    },
    textContent:{
        fontSize: 20,
        color: '#ffffff',
        padding: 35,
        paddingBottom: 10,
        paddingTop: 15
    },

    modalContainer:{
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        padding: 25,
        margin:20,
        borderWidth: 1,
        borderColor: '#f2f2f2',
        borderRadius: 15
    },
    modalImage:{
        padding: 30
    },
    modalText:{
        fontSize: 17,
        color: '#2C2C2C'
    },
    closeBtnContainer:{
        padding: 20
    },
    closeBtn:{
        width: 40,
        height: 40
    }
});

AppRegistry.registerComponent('project', () => DataBooks);